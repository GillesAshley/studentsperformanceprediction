from django import forms

'''SCH_CHOICES = [
    ('BG', 'Bishop Gates'),
    ('QC', 'Queens Court'),
    ('KC', 'Kings Court'),
]'''

SEX_CHOICES = [
    ('M', 'Male'),
    ('F', 'Female'),
]

# Family Size
FAMSIZE_CHOICES = [
    ('LE3', 'Less Than 3'),
    ('GT3', 'Greater Than 3')
]

# Parent's Cohabitation Status
PSTATUS_CHOICES = [
    ('T', 'Together'),
    ('A', 'Apart'),
]

# Mother's Education
MEDU_CHOICES = [
    ('0', 'None'),
    ('1', 'Primary Education 4th Grade'),
    ('2', '5th to 9th Grade'),
    ('3', 'Secondary Education'),
    ('4', 'Higher Education')
]

# Fathers's Education
FEDU_CHOICES = [
    ('0', 'None'),
    ('1', 'Primary Education 4th Grade'),
    ('2', '5th to 9th Grade'),
    ('3', 'Secondary Education'),
    ('4', 'Higher Education')
]

# Mother's Job
MJOB_CHOICES = [
    ('teaching', 'teaching'),
    ('health', 'health'),
    ('services', 'Civil Services'),
    ('home', 'At Home'),
    ('other', 'Other')
]

# Father's Job
MJOB_CHOICES = [
    ('teaching', 'teaching'),
    ('health', 'health'),
    ('services', 'Civil Services'),
    ('home', 'At Home'),
    ('other', 'Other')
]

# Prediction Form
class StudentDataForm(forms.Form):
    school = forms.CharField(label='Student Sch:', max_length=100)
    sex = forms.ChoiceField(choices = SEX_CHOICES)
    age = forms.IntegerField(label='Age:')
    address = forms.CharField(label='Address:', max_length=100)
    famsize = forms.ChoiceField(choices=FAMSIZE_CHOICES,label='Family Size:')
    pStatus = forms.ChoiceField(choices=PSTATUS_CHOICES,label='Parent status')
    '''mEdu 
    fEdu
    mJob
    fJob
    reason
    guardian
    travel_time
    study_time
    weekly_study_time
    failures
    school_sup
    fam_sup
    paid
    activities
    nursery
    higher
    internet
    romantic
    fam_rel
    free_time
    go_out
    day_alc
    week_alc
    health
    absenses
    G_one
    G_two
    G_three'''

#Upload Form
class UploadForm(forms.Form):
    file = forms.FileField(label="Upload File Here ",required=True)
    



