from django.shortcuts import render
import os
from predict.modules.helpers import Helpers
from .forms import UploadForm

## Create a table for the shape of the data to be processed
import matplotlib
matplotlib.use("TKAgg")
import matplotlib.pyplot as plt, mpld3
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from keras.utils import np_utils
from keras import regularizers
from keras.models import Sequential,load_model
from keras.utils.np_utils import to_categorical
from sklearn import preprocessing
from keras.layers import Dense, Dropout




utils = Helpers()
def index(request):
    return render(request, 'predict/index.html')


def upload(request):
    uploadForm = UploadForm(request.POST or None, request.FILES or None)
    student_total_record = None
    student_preview_count = None
    student_tableu = None
    student_tableu_desc = None
    model_accuracy1 = None
    model_accuracy2 = None
    model_accuracy3 = None
    mapping = None
    plt_1 = None
    g3_validation_accuracy = None
    g3_validation_loss = None
    g2_validation_accuracy = None
    g2_validation_loss = None
    g1_validation_accuracy = None
    g1_validation_loss = None
    if request.method == "POST":
         if uploadForm.is_valid():
             if utils.path_exists("datafiles"):
                with open("datafiles/records.csv", 'wb+') as file:
                    for chunk in request.FILES['file'].chunks():
                        file.write(chunk)  
                #Processing the files here
                Student_Math_Data = pd.read_csv('datafiles/records.csv',delimiter =';')
                #Count the first previewed records
                student_total_record = Student_Math_Data.__len__()
                student_preview_count = Student_Math_Data.head(10).__len__()
                # Table for the list of records - first 10
                student_tableu = Student_Math_Data.head(10).to_html()
                # Table description for the list of records - first 10
                student_tableu_desc = Student_Math_Data.describe().to_html()
                my_colors = 'rgbkymc'
                cat_variables = []
                for i in Student_Math_Data.select_dtypes(['object']).columns:
                    my_tab = pd.crosstab(index = Student_Math_Data[i], columns="count")  
                    cat_variables.append(my_tab.to_json())  
                #print(cat_variables) 
                    #my_tab.plot.bar(color=my_colors) 
                #Calculate the correlation in order to know which variables are most related
                correlation = Student_Math_Data.corr()
                correlation_sorted = correlation.abs().unstack()
                correlation_sorted.sort_values(ascending = False)

                # create a copy of the original data record
                Student_Math_Data_Copy = Student_Math_Data
                #  Convert the G3 row to five leter grades A...F
                grades = utils.class_grades(Student_Math_Data['G3'])
                # Get series from the  grades
                se = pd.Series(grades)
                # Associate the records grade column with the pd series values
                assocs = Student_Math_Data_Copy['Grade'] = se.values
                mapping = Student_Math_Data_Copy.head(2).to_html()
                # check for any non unique columns
                Student_Math_Data_Copy.apply(pd.Series.nunique)
                #Check if any columns has NaN's
                Student_Math_Data.isnull().sum()
                #Now drop G3 column
                student_Math_Data_without_G3 =  Student_Math_Data_Copy.drop(['G3'], axis=1)
                #Create data frames for input and output variables
                Y = student_Math_Data_without_G3.filter(["Grade"],axis=1)
                X = student_Math_Data_without_G3.drop(['Grade'],axis=1)
                #split the datasets into train and test
                xTrain, xTest, yTrain, yTest = train_test_split(X, Y, test_size=0.15)
                #Perform one hot encoding
                x_train = pd.get_dummies(xTrain)
                x_test = pd.get_dummies(xTest)
                y_train  = pd.get_dummies(yTrain)
                y_test  = pd.get_dummies(yTest)
                print(mapping)
                # Performing multiclass classification
                if utils.path_exists("models"):
                   history = utils.classification('models/UCI_model_A.h5',x_train.values, y_train.values, x_test.values, y_test.values)
            
                #Load the saved model and predict the accuracy
                model_1 = load_model('models/UCI_model_A.h5')
                model_1.summary()
                results = model_1.evaluate(x_test.values,y_test.values)
                model_accuracy1 = "{:.2f}%".format(results[1]*100)
               #Predict Y values
                y_predicted = model_1.predict(x_test)
                test_Y = utils.predict_y(y_test)
                predicted_Y1 = utils.predicted_y1(y_predicted)
                # Generating the confusion matrix values here
                names = ["Grade_A", "Grade_B", "Grade_C","Grade_D","Grade_F"]
                cm = confusion_matrix(test_Y, predicted_Y1,labels=["A", "B", "C","D","F"])
                print(cm)
                series = []
                # Getting the accuracy of the validation epochs
                accuracy = history.history['acc']
                validation_accuracy = history.history['val_acc']
                epochs = range(1, len(accuracy)+ 1)
                xAxis = []
                for i in epochs:
                    xAxis.append(i)
                fig = plt.figure(figsize=(8.9,6.8),edgecolor='black')
                fig.subplots_adjust(top=0.6)
                ax1 = fig.add_axes([0.15,0.1,0.7,0.3])
                ax1.set_xlabel("Epochs")
                ax1.set_ylabel("Accuracy")
                ax1.set_title("Plot of Validation and Training Accuracy")
                ax1.plot(xAxis, accuracy, lw=2, marker='o', label="Training Accuracy")
                ax1.plot(xAxis, validation_accuracy, lw=2, color="orange", label="Validation Accuracy")
                ax1.legend()
                g3_validation_accuracy = mpld3.fig_to_html(fig)
                #utils.validation_graph("Plot of Validation and Training Accuracy","Epochs",accuracy, "Accuracy", validation_accuracy, "Training Accuracy", "Validation Accuracy")

                # Getting the accuracy of the validation losses using the epochs
                #Plot training loss and validation loss
                loss = history.history['loss']
                val_loss = history.history['val_loss']
                epochsLoss  = range(1,len(loss)+1)
                lossAxis = []
                for i in epochsLoss:
                    lossAxis.append(i)

                fig = plt.figure(figsize=(8.9,6.8),edgecolor='black')
                fig.subplots_adjust(top=0.6)
                ax1 = fig.add_axes([0.15,0.1,0.7,0.3])
                ax1.set_xlabel("Epochs")
                ax1.set_ylabel("Loss")
                ax1.set_title("Plot of Validation and Training Loss")
                ax1.plot(lossAxis, loss, lw=2, marker='o', label="Training Loss")
                ax1.plot(lossAxis, val_loss, lw=2, color="orange", label="Validation Loss")
                ax1.legend()
                g3_validation_loss = mpld3.fig_to_html(fig)


                #Now drop G2 column
                student_Math_Data_without_G2 =  Student_Math_Data_Copy.drop(['G2'], axis=1)
                #Create data frames for input and output variables
                Y_G2 = student_Math_Data_without_G2.filter(["Grade"],axis=1)
                X_G2 = student_Math_Data_without_G2.drop(['Grade'],axis=1)
                #Split the dataframes to train and test
                xTrain_G2, xTest_G2, yTrain_G2, yTest_G2 = train_test_split(X_G2, Y_G2, test_size=0.15)
                #Perform one hot encoding of categorical variables
                x_train_G2 = pd.get_dummies(xTrain_G2)
                x_test_G2 = pd.get_dummies(xTest_G2)
                y_train_G2  = pd.get_dummies(yTrain_G2)
                y_test_G2  = pd.get_dummies(yTest_G2)
                # Performing multiclass classification
                if utils.path_exists("models"):
                   history_G2 = utils.classification('models/UCI_model_B.h5',x_train_G2.values, y_train_G2.values, x_test_G2.values, y_test_G2.values)
                   print("history")
                   print(history_G2)
                model_2 = load_model('models/UCI_model_B.h5')
                model_2.summary()
                results2 = model_2.evaluate(x_test_G2.values,y_test_G2.values)
                model_accuracy2 = "{:.2f}%".format(results2[1]*100)
                # Save the model accuracy in the db
                #Predict Y values
                y_predicted_G2 = model_2.predict(x_test_G2)
                #Predict Y values
                test_Y_G2 = utils.predict_y(y_test_G2)
                predicted_Y_G2 = utils.predicted_y1(y_predicted_G2)
                #Plot training accuracy and validation accuracy
                acc_G2 = history_G2.history['acc']
                val_acc_G2 = history_G2.history['val_acc']   
                fig = plt.figure(figsize=(8.9,6.8),edgecolor='black')
                fig.subplots_adjust(top=0.6)
                ax1 = fig.add_axes([0.15,0.1,0.7,0.3])
                ax1.set_xlabel("Epochs")
                ax1.set_ylabel("Accuracy")
                ax1.set_title("Plot of Validation and Training Accuracy")
                ax1.plot(xAxis, acc_G2, lw=2, marker='o', label="Training Accuracy")
                ax1.plot(xAxis, val_acc_G2, lw=2, color="orange", label="Validation Accuracy")
                ax1.legend()
                g2_validation_accuracy = mpld3.fig_to_html(fig)

                 #Plot training accuracy and validation accuracy
                loss_G2 = history_G2.history['loss']
                val_loss_G2 = history_G2.history['val_loss']   
                fig = plt.figure(figsize=(8.9,6.8),edgecolor='black')
                fig.subplots_adjust(top=0.6)
                ax1 = fig.add_axes([0.15,0.1,0.7,0.3])
                ax1.set_xlabel("Epochs")
                ax1.set_ylabel("Accuracy")
                ax1.set_title("Plot of Validation and Training Loss")
                ax1.plot(lossAxis, loss_G2, lw=2, marker='o', label="Training Loss")
                ax1.plot(lossAxis, val_loss_G2, lw=2, color="orange", label="Validation Loss")
                ax1.legend()
                g2_validation_loss = mpld3.fig_to_html(fig)

                #Now drop G2 column
                student_Math_Data_without_G1 =  Student_Math_Data_Copy.drop(['G1'], axis=1)
                #Create data frames for input and output variables
                Y_G1 = student_Math_Data_without_G1.filter(["Grade"],axis=1)
                X_G1 = student_Math_Data_without_G1.drop(['Grade'],axis=1)
                #Split the dataframes to train and test
                xTrain_G1, xTest_G1, yTrain_G1, yTest_G1 = train_test_split(X_G1, Y_G1, test_size=0.15)
                #Perform one hot encoding of categorical variables
                x_train_G1 = pd.get_dummies(xTrain_G1)
                x_test_G1 = pd.get_dummies(xTest_G1)
                y_train_G1  = pd.get_dummies(yTrain_G1)
                y_test_G1 = pd.get_dummies(yTest_G1)
                # Performing multiclass classification
                if utils.path_exists("models"):
                   history_G1 = utils.classification('models/UCI_model_C.h5',x_train_G1.values, y_train_G1.values, x_test_G1.values, y_test_G1.values)
                model_3 = load_model('models/UCI_model_C.h5')
                model_3.summary()
                results2 = model_3.evaluate(x_test_G1.values,y_test_G1.values)
                model_accuracy3 = "{:.2f}%".format(results2[1]*100)
                # Save the model accuracy in the db
                #Predict Y values
                y_predicted_G1 = model_2.predict(x_test_G1)
                #Predict Y values
                test_Y_G1 = utils.predict_y(y_test_G1)
                predicted_Y_G1 = utils.predicted_y1(y_predicted_G1)
                #Plot training accuracy and validation accuracy
                acc_G1 = history_G1.history['acc']
                val_acc_G1 = history_G1.history['val_acc']   
                fig = plt.figure(figsize=(8.9,6.8),edgecolor='black')
                fig.subplots_adjust(top=0.6)
                ax1 = fig.add_axes([0.15,0.1,0.7,0.3])
                ax1.set_xlabel("Epochs")
                ax1.set_ylabel("Accuracy")
                ax1.set_title("Plot of Validation and Training Accuracy")
                ax1.plot(xAxis, acc_G1, lw=2, marker='o', label="Training Accuracy")
                ax1.plot(xAxis, val_acc_G1, lw=2, color="orange", label="Validation Accuracy")
                ax1.legend()
                g1_validation_accuracy = mpld3.fig_to_html(fig)

                #Plot training accuracy and validation accuracy
                loss_G1 = history_G1.history['loss']
                val_loss_G1 = history_G1.history['val_loss']   
                fig = plt.figure(figsize=(8.9,6.8),edgecolor='black')
                fig.subplots_adjust(top=0.6)
                ax1 = fig.add_axes([0.15,0.1,0.7,0.3])
                ax1.set_xlabel("Epochs")
                ax1.set_ylabel("Accuracy")
                ax1.set_title("Plot of Validation and Training Loss")
                ax1.plot(lossAxis, loss_G1, lw=2, marker='o', label="Training Loss")
                ax1.plot(lossAxis, val_loss_G1, lw=2, color="orange", label="Validation Loss")
                ax1.legend()
                g1_validation_loss = mpld3.fig_to_html(fig)
                
                
    else:
       uploadForm = UploadForm()
  
    context = {
        "uploadForm": uploadForm,
        "total_record_count": student_total_record,
        "preview_count": student_preview_count,
        'student_tableu': student_tableu,
        'student_tableu_desc': student_tableu_desc,
        'records_mapping': mapping,
        'accuracy_model1': model_accuracy1,
        'g3_validation_accuracy': g3_validation_accuracy,
        "g3_validation_loss":g3_validation_loss,
        'model_accuracy2':model_accuracy2,
        "g2_validation_accuracy": g2_validation_accuracy,
        'g2_validation_loss': g2_validation_loss,
        'model_accuracy3':model_accuracy3,
        'g1_validation_accuracy': g1_validation_accuracy,
        'g1_validation_loss': g1_validation_loss
    }
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    DATAFILES = "/Users/gillesashley/Desktop/studentsperformanceprediction/datafiles/"
    MODELS = "/Users/gillesashley/Desktop/studentsperformanceprediction/models/"
    def removeFiles(path):  
        for filename in os.listdir(path):
            file_path = os.path.join(path, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
            except Exception as e:    
                print("Failed to delete %s. Reason: %s" % (file_path, e))
    removeFiles(DATAFILES)
    removeFiles(MODELS)

    return render(request, 'predict/upload.html', context)
