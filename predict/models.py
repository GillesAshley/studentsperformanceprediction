from django.db import models

# Create your models here.


# Student Model
class Student(models.Model):
    length = 20
    school = models.CharField(help_text='Student Sch:', max_length=100)
    sex = models.CharField(help_text='Sex:', max_length=10)
    age = models.IntegerField(help_text='Age:', default="5")
    address = models.CharField(help_text='Address:', max_length=100)
    famsize = models.CharField(help_text='Family Size:', max_length=100)
    pStatus = models.CharField(help_text='', max_length=100)
    mEdu = models.CharField(max_length=length, blank=True)
    fEdu = models.CharField(max_length=length, blank=True)
    mJob = models.CharField(max_length=length, blank=True)
    fJob = models.CharField(max_length=length, blank=True)
    reason = models.CharField(max_length=length, blank=True)
    guardian = models.CharField(max_length=length, blank=True)
    travel_time = models.CharField(max_length=length, blank=True)
    study_time = models.CharField(max_length=length, blank=True)
    weekly_study_time = models.CharField(max_length=length, blank=True)
    failures = models.CharField(max_length=length, blank=True)
    school_sup = models.CharField(max_length=length, blank=True)
    fam_sup = models.CharField(max_length=length, blank=True)
    paid = models.CharField(max_length=length, blank=True)
    activities = models.CharField(max_length=length, blank=True)
    nursery = models.CharField(max_length=length, blank=True)
    higher = models.CharField(max_length=length, blank=True)
    internet = models.CharField(max_length=length, blank=True)
    romantic = models.CharField(max_length=length, blank=True)
    fam_rel = models.CharField(max_length=length, blank=True)
    free_time = models.CharField(max_length=length, blank=True)
    go_out = models.CharField(max_length=length, blank=True)
    day_alc = models.CharField(max_length=length, blank=True)
    week_alc = models.CharField(max_length=length, blank=True)
    health = models.CharField(max_length=length, blank=True)
    absenses = models.CharField(max_length=length, blank=True)
    G_one = models.IntegerField(max_length=length, blank=True)
    G_two = models.IntegerField(max_length=length, blank=True)
    G_three = models.IntegerField(max_length=length, blank=True)

    def __str__(self):
        return '%s' % (self.school)

