import os
import shutil
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from keras.utils import np_utils
from keras import regularizers
from keras.models import Sequential,load_model
from keras.utils.np_utils import to_categorical
from sklearn import preprocessing
from keras.layers import Dense, Dropout
import matplotlib
matplotlib.use("TKAgg")
import matplotlib.pyplot as plt, mpld3

class Helpers(object):
    def path_exists(self,path: str) -> bool:
        if os.path.exists(path) is False:
            os.mkdir(path)
            return True
        else:
            return True    
    
    def class_grades(self, records: list) -> list:
        #As instructed in the paper, convert G3 to 5 grade classes A,B,C,D,F
        grade = []
        for i in records.values:
            if i in range(0,10):
                grade.append('F')
            elif i in range(10,12):
                grade.append('D')
            elif i in range(12,14):
                grade.append('C')
            elif i in range(14,16):
                grade.append('B')
            else:
                grade.append("A")
                pass
        return grade

    def one_hot_encoding(self, record):
        return pd.get_dummies(record)
    def classification(self,filepath, x_train, y_train, x_test, y_test, epochs = 20, batch_size = 5): 
        #After multiple attempts, I was able to achieve best model with below configuration.
        model = Sequential()
        model.add(Dense(64, activation='relu',  kernel_regularizer=regularizers.l2(0.001),input_shape = (58,)))
        model.add(Dense(64, kernel_regularizer=regularizers.l2(0.001), activation='relu'))
        model.add(Dense(32, kernel_regularizer=regularizers.l2(0.001), activation='relu'))
        model.add(Dense(5,  kernel_regularizer=regularizers.l2(0.001),activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        history  = model.fit(x_train,y_train, epochs = epochs, batch_size = batch_size, validation_data = (x_test,y_test))
        model.save(filepath)
        return history
    
    def predict_y(self, y_records) -> list:
        #Convert y_test dataframe to letter grades
        test_Y = []
        for i in y_records.values:
            if i[4].round() ==1:
               test_Y.append('F')
            elif i[3].round() ==1:
                test_Y.append('D')
            elif i[2].round() == 1:
                test_Y.append('C')
            elif i[1].round() ==1:
                test_Y.append('B')
            elif i[0].round() ==1:
                test_Y.append('A')
        return test_Y
    
    def predicted_y1(self, y_predicted):
        #convert y_predicted dataframe to letter grades
        predicted_Y1 = []
        for i in y_predicted:
            if i[4].round() == 1:
                predicted_Y1.append('F')
            elif i[3].round() ==1:
                predicted_Y1.append('D')
            elif i[2].round() == 1:
                predicted_Y1.append('C')
            elif i[1].round() ==1:
                predicted_Y1.append('B')
            else:
                predicted_Y1.append('A')
        return predicted_Y1
    

    def validation_graph(title,xlabel,xData, ylabel,yData,epochs,label1, label2):
        fig = plt.figure(figsize=(8.9,6.8),edgecolor='black')
        fig.subplots_adjust(top=0.6)
        ax1 = fig.add_axes([0.15,0.1,0.7,0.3])
        ax1.set_xlabel(xlabel)
        ax1.set_ylabel(ylabel)
        ax1.set_title(title)
        ax1.plot(epochs, xData, lw=2, marker='o', label=label1)
        ax1.plot(epochs, yData, lw=2, color="orange", label=label2)
        ax1.legend()
        return mpld3.fig_to_html(fig)
    
    def removeFiles(path):
        for filename in os.listdir(path):
            file_path = os.path.join(path, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
            except Exception as e:    
                print("Failed to delete %s. Reason: %s" % (file_path, e))